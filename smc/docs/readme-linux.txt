Guides are now on the wiki :

Installing from Package Manager : 
    archive: https://web.archive.org/web/20151201160924/http://www.secretmaryo.org/wiki/index.php?title=Installing_from_Package
	dead link: http://www.secretmaryo.org/wiki/index.php?title=Installing_from_Package
Compiling from Tarball : 
	archive: https://web.archive.org/web/20180218120827/http://www.secretmaryo.org/wiki/index.php?title=Compiling_from_Tarball
	dead link: http://www.secretmaryo.org/wiki/index.php?title=Compiling_from_Tarball

------------------------------------------------------------------------

(The original Secret Maryo Chronicles is at https://github.com/FluXy/SMC )

Dogio - Unofficial building instructions Ubuntu-based systems(2019):

1. Install build tools and dependency packages

    sudo apt-get update
    sudo apt-get install automake gettext git build-essential libboost-filesystem-dev libboost-thread-dev libcegui-mk2-dev libsdl1.2-dev libsdl-ttf2.0-dev libsdl-mixer1.2-dev libsdl-image1.2-dev libglu1-mesa-dev libgl1-mesa-dev

2. Build GEGUI 0.7 if needed. Modern sources no longer provide versions before 0.8. Here we will build and install CEGUI v0.7.9.

    sudo apt-get install mercurial
    hg clone ssh://hg@bitbucket.org/cegui/cegui -r v0-7-9
    cd cegui
    ./bootstrap
    ./configure --enable-null-renderer
    make
    make install
    sudo ldconfig

3. Build SMC and install to a particular directory (the prefix directory). 

	git clone https://bitbucket.org/SoCo_cpp/dogio.git
    
    cd dogio
    cd smc
    ./autogen.sh
    ./configure --prefix=/home/user/smc
    make
    make install

4. To run, you execute the 'smc' executable in the bin folder. 
   With the provided prefix it will be:
    /home/user/smc/bin/smc

------------------------------------------------

Tips:

 Problem:
	hg (mercurial) can't clone GEGUI using the ssh repository link above
 Solution:
	Try the HTTPS repository link instead https://bitbucket.org/cegui/cegui 
	If you get SSL errors, your ssh may be defaulting to old TLS.
	To remedy this, create ~/.hgrc with the two lines that follow:
	   [ui]
	   tls = False	 

 Problem:
   Need to rebuild everything cleanly.
 Solution:
    make clean
    make distclean

 Problem:
   What is the hg command?
 Solution:
   That is the mercurial current version system's executable (similar to git).

 Problem:
    mc-video.o: undefined reference to symbol 'pthread_condattr_setclock@@GLIBC_2.3.3'
 Solution:
   Before running ./configure, explicitly add linking libpthread to the linker flags
    export LIBS="-lpthread"

 Problem:
   Linker errors for missing CEGUI 'render' symbols.
 Solution:
   GEGUI may not have been built with rederer options enable.
   See the --enable-null-renderer parameter given to ./configure

 Problem:
   I've compiled SMC, but get errors of missing CEGUI libraries when I run it.
 Solution:
   Ensure you preformed a ldconfig after installing CEGUI.
    sudo ldconfig

 Problem:
   I don't see autogen.sh
 Solution:
   autogen.sh is used when building from the git repository. 
   Zip or tar source code bundles typically come without autogen.sh and with this step already done.
   It generates the configure script. Check if your configure script already exist.
   autogen.sh and configure both go in the source code directory ./dogio/smc/
   
