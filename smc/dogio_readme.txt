Dogio
-----

Dogio is a Dogecoin theme for Secret Maryo Chronicles.

A Dogio datapack contains just the data files of the Dogio theme.
It must be extracted into your existing Secret Maryo Chronicles'
directory. This is the directory with game's program in it and 
the 'data' direcotry.


Secret Maryo Chronicles
-----------------------

Secret Maryo Chronicles is an Open Source two-dimensional platform
game with a design similar to classic computer games.

http://www.secretmaryo.org


Dogecoin 
--------

Dogecoin is the open source peer-to-perer crypto currency, favored by Shiba Inus worldwide!

https://dogecoin.com
