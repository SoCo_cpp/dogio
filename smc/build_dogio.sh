DOGIO_VERSION_STRING="0.2"
TARGET_DIR="./dogio/"

[ ! -d "dogio" ] && mkdir dogio

TARGET_FILENAME="${TARGET_DIR}dogio_datapack_${DOGIO_VERSION_STRING}"
DOGIO_FILE_LIST=`sed -e :a -e '$!N; s/\n/ /; ta' ./dogio_file_list`

zip "${TARGET_FILENAME}.zip" -@ < ./dogio_file_list
tar -cvzf "${TARGET_FILENAME}.tar.gz" -T./dogio_file_list
7za a "${TARGET_FILENAME}.7z" -i\@dogio_file_list

echo ""
echo "Dogio packages built. Find them in the 'dogio' directory."


