Dogio
=====

NOTE
----

Dogio is a working name. This project is in an early exploratory stage.


What is it?
-----------

This project wants to create a Dogecoin themed version for Secret Maryo Chronicles.

status
------

General tasks:

 * clone SMC into a git project.(completed: https://bitbucket.org/SoCo_cpp/dogio/)
 * quick first draft character images for small, big, and fire (started)
 * quick first draft coin and pineapple items (started)
 * splash, background, menu, and additional theming considerations
 * moon (wow!)


Secret Maryo Chronicles
=======================

NOTE
----

SMC is no longer under active development. However, there is now a
new game based on SMC:

https://github.com/secretchronicles/TSC

What is it?
-----------

From the official website at http://www.secretmaryo.org:

> Secret Maryo Chronicles is an Open Source two-dimensional platform
> game with a design similar to classic computer games. SMC has
> computer support to a great degree by using an accelerated [Open
> Graphics Library](http://opengl.org) for the best possible graphic
> design and stock performance.

It features a wealth of levels, powerups, great music and much more �
see http://www.secretmaryo.org/index.php?page=about for a more
comprehensive list. And for those still not getting enough, it
provides a great in-game level editor.

License
-------

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
